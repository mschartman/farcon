If you have downloaded this code and want to run it on your own system, make sure you have installed everything mentioned in the project homepage:

https://foo.cs.ucsb.edu/cs1a/index.php/CS1L:F11:MattSchartman

The GIT repository for the code can be found at:

https://bitbucket.org/mschartman/farcon/src

To run the code, just make your current working directory the root directory of the project and run

python manage.py runserver

If the database is not built you will first have to run

python manage.py syncdb

If you want to scrap the database provided and rebuild it, simply delete the database file and Django will recreate it for you from scratch based off of the models when you run the code. Then do the two steps mentioned above to run the application.

